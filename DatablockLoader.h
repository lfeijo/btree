//
// Created by Lucas Feijo on 5/15/16.
//

#ifndef V1_DATABLOCKLOADER_H
#define V1_DATABLOCKLOADER_H


#include "Datablocks/BLeafBlock.h"
#include "Datablocks/MasterBlock.h"
#include "DatablockBuffer.h"

class DatablockLoader {

public:
    static MasterBlock * getMasterblock();

    static bool newDatafile(char *datafileName);
    static bool loadDatafile(char * datafileName);

    // Salva datablock no buffer
    static bool saveDatablock(Datablock *datablock);

    // Persiste todos os datablocks do buffer no disco.
    static bool flushBuffer();

    // (NAO USAR) Salva datablock no disco (NAO USAR)
    // Esse metodo deve ser usado apenas PELO BUFFER no caso de write back.
    static bool persistDatablock(Datablock *datablock);

    // Busca um datablock primeiramente do buffer, se nao encontrado busca do disco.
    // O parametro type eh opcional, caso o datablock ja possua um tipo
    // Se for necessario impor um tipo sobre ele, passar no type e ele sera lido como se fosse outro tipo.
    static Datablock * loadDatablock(int id, Datablock::BlockTypes type = Datablock::empty);

    // O datablock onde sao armazenadas informacoes de controle sobre o datafile, sempre id=0.
    static MasterBlock * masterBlock;

    // retorna vetor com todos os ids de datablocks utilizados
    // retorna vazio caso o datafile nao esteja configurado ainda
    static std::set<uint16_t> getDatablockIds();

    // loada um datablock nao utilizado
    // retorna NULL caso nao consiga mais nenhum
    static Datablock *loadNewDatablock(Datablock::BlockTypes type);

private:

    static DatablockBuffer datablockBuffer;
    static bool hasLoadedDatafile;
    static int blockQuant;
    static char * datafileAddr;
    static Datablock * assembleBlock(int id, char *input, Datablock::BlockTypes type = Datablock::empty);
};


#endif //V1_DATABLOCKLOADER_H
