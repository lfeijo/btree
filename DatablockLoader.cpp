//
// Created by Lucas Feijo on 5/15/16.
//

#include <cstdlib>
#include <unistd.h>
#include <set>
#include <algorithm>
#include "DatablockLoader.h"
#include "Datablocks/BBranchBlock.h"
#include "Datablocks/TableBlock.h"
#include "DatablockBuffer.h"

bool DatablockLoader::hasLoadedDatafile = false;
int DatablockLoader::blockQuant = 15625;
char * DatablockLoader::datafileAddr = "datafile.bin";
MasterBlock * DatablockLoader::masterBlock = NULL;
DatablockBuffer DatablockLoader::datablockBuffer;

bool DatablockLoader::newDatafile(char *name) {
    FILE *pFile;
    pFile = fopen(name, "wb");
    if (pFile != NULL) {
        datafileAddr = name;
        for (int i = 0; i < blockQuant; ++i) {
            fseek(pFile, i * Datablock::size, SEEK_SET);
            fputc(NULL, pFile);
            fflush(pFile);
        }
        fclose(pFile);
        hasLoadedDatafile = true;
        saveDatablock(new MasterBlock());
        masterBlock = getMasterblock();
        return true;
    } else {
        return false;
    }
}

bool DatablockLoader::loadDatafile(char * name) {
    FILE *pFile = fopen(name, "rb");
    if (pFile!=NULL) {
        datafileAddr = name;
        hasLoadedDatafile = true;
        masterBlock = getMasterblock();
        return true;
    } else {
        return false;
    }
}

MasterBlock * DatablockLoader::getMasterblock() {
    return (MasterBlock*)loadDatablock(0);
}

Datablock * DatablockLoader::loadDatablock(int id, Datablock::BlockTypes type) {
    if (hasLoadedDatafile) {
        Datablock *block = datablockBuffer.getDatablock(id);
        if (block != NULL) { // verifica buffer primeiro
            return block;
        } else { // nao se encontra no buffer, buscar do datafile
            FILE *pFile;
            char *data = (char *)calloc(1, Datablock::size);
            pFile = fopen(datafileAddr, "rb");
            fseek(pFile, id * Datablock::size, SEEK_SET); // avanca ate bloco especifico
            fread(data, Datablock::size, 1, pFile);
            fclose(pFile);
            Datablock *datablock = assembleBlock(id, data, type);
            free(data);
            datablockBuffer.storeDatablock(datablock); // salva em ordem FIFO
            return datablock;
        }
    } else {
        return NULL;
    }
}

bool DatablockLoader::saveDatablock(Datablock *datablock) {
    if (hasLoadedDatafile) {
        datablockBuffer.storeDatablock(datablock);
        return true;
    } else return false;
}

bool DatablockLoader::persistDatablock(Datablock *datablock) {
    if (hasLoadedDatafile) {
        FILE *pFile;
        uint16_t id = datablock->id;
        char *data = datablock->getSerialized();
        pFile = fopen(datafileAddr, "r+b");
        fseek(pFile, id * Datablock::size, SEEK_SET);
        fwrite(data, Datablock::size, 1, pFile);
        fclose(pFile);
        free(data);
        return true;
    } else {
        return false;
    }
}

Datablock * DatablockLoader::assembleBlock(int id, char *input, Datablock::BlockTypes type) {
    uint8_t t;
    if (type!=Datablock::empty) t = type;
    else t = (uint8_t)input[0];
    switch(t) {
        case Datablock::master:
            return new MasterBlock(input);
        case Datablock::leaf:
            return new BLeafBlock(id, input);
        case Datablock::branch:
            return new BBranchBlock(id, input);
        case Datablock::data:
            return new TableBlock(id, input);
    }
    printf("ERR Could not define block type for id %d.\n", id);
    return NULL;
}

std::set<uint16_t> DatablockLoader::getDatablockIds() {
    if (hasLoadedDatafile) {
        return masterBlock->datablocks;
    }
    return std::set<uint16_t>();
}

Datablock *DatablockLoader::loadNewDatablock(Datablock::BlockTypes type) {
    std::set<uint16_t> setAll;
    for (int i = 1; i < Datablock::quant; ++i) { // insere todos ids de datablock possiveis
        setAll.insert(i);
    }
    std::set<uint16_t> setExisting = DatablockLoader::getDatablockIds();
    std::set<uint16_t> setDiff;
    std::set_difference(setAll.begin(), setAll.end(), setExisting.begin(), setExisting.end(),
                        std::inserter(setDiff, setDiff.end()));
    if (setDiff.size()>0) {
        uint16_t id = *setDiff.begin(); // pega qualquer datablock livre
        Datablock * block = DatablockLoader::loadDatablock(id, type);
        return block;
    } else { // nao sobrou espaco para criar datablocks
        return NULL;
    }
}

// Por padrao na escrita (save) de um datablock ele apenas eh colocado no buffer
// Eh necessario dar flush ou acabar o espaco no buffer para ele ser persistido
bool DatablockLoader::flushBuffer() {
    if (hasLoadedDatafile) {
        return datablockBuffer.flush();
    } else return false;
}





