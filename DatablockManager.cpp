//
// Created by Lucas Feijo on 5/23/16.
//

#include <set>
#include <algorithm>
#include "DatablockManager.h"
#include "DatablockLoader.h"

int DatablockManager::idCounter = 1;
//BTree * DatablockManager::btree = new BTree();

TableBlock *DatablockManager::findFit(uint8_t entrySize) {
    TableBlock *block;
    std::set<uint16_t> ids = DatablockLoader::getDatablockIds();
    for (auto id : ids) {
        block = (TableBlock *) DatablockLoader::loadDatablock(id);
        if (block!=NULL) {
            if (block->type == Datablock::data) {
                if (((TableBlock *) block)->doesFit(entrySize)) {
                    return (TableBlock *) block;
                }
            }
        }
    }
    // nao cabe em nenhum datablock existente, criar um novo
    block = (TableBlock *) DatablockLoader::loadNewDatablock(Datablock::data);
    return block;
}

int findId()
{
    std::set<uint16_t> setAll;
    for (int i = 1; i < Datablock::quant; ++i) { // insere todos ids de datablock possiveis
        setAll.insert(i);
    }
    std::set<uint16_t> setExisting = DatablockLoader::getDatablockIds();
    std::set<uint16_t> setDiff;
    std::set_difference(setAll.begin(), setAll.end(), setExisting.begin(), setExisting.end(),
                        std::inserter(setDiff, setDiff.end()));
    if (setDiff.size()>0) {
        uint16_t id = *setDiff.begin(); // pega qualquer datablock livre
        return id;
    } else { // nao sobrou espaco para criar datablocks
        return -1;
    }
}



uint16_t DatablockManager::addEntry(uint32_t pk, std::string value)
{
    BTree btree = BTree();
    TableBlock * tableBlock = findFit(value.size());
    if (tableBlock!=NULL) {
        int rowid = tableBlock->addEntry(pk, value);
        if (rowid != -1) {
            if (DatablockLoader::saveDatablock(tableBlock)) {
                uint32_t meme = tableBlock->id << 16;
                meme += rowid;
                btree.addEntry(pk, meme);
                DatablockLoader::masterBlock->datablocks.insert(tableBlock->id);
                return tableBlock->id;
            } else {
                printf("ERR could not save datablock %d.\n", tableBlock->id);
                return 0;
            }
        } else {
            printf("ERR could not add entry to datablock %d.\n", tableBlock->id);
            return 0;
        }
    } else {
        printf("ERR could not find a datablock to fit entry.\n");
        return 0;
    }
}

bool DatablockManager::removeEntry(int index)
{
    BTree btree = BTree();
    bool succeed = btree.removeEntry(index);
    if(!succeed) {
        printf("ERR could not delete %d value.\n", index);
    }


}

std::vector<std::tuple<uint32_t, uint32_t, std::string>> DatablockManager::findByValue(std::string value) {
    std::set<uint16_t> ids = DatablockLoader::getDatablockIds();
    std::vector<std::tuple<uint32_t, uint32_t, std::string>> allResults;
    for (auto id : ids) { // para todos datablocks de dados
        Datablock * block = DatablockLoader::loadDatablock(id);
        TableBlock * tblock;
        if (block->type == Datablock::data) {
            tblock = (TableBlock*)block;
            std::vector<std::tuple<uint32_t, uint32_t, std::string>> results = tblock->findEntries(value);
            if (results.size()>0) {
                 for (auto r : results) { // para todos resultados identificados no datablock atual
                     allResults.push_back(r);
                 }
            }
        }
    }
    return allResults;
}

std::string DatablockManager::findByIndex(int index)
{
    BTree btree = BTree();
    return btree.findRegistry(index);
}

int DatablockManager::getUniqueId()
{
    return findId();
}



