//
// Created by Lucas Feijo on 5/15/16.
//

#include <cstdlib>
#include <iostream>
#include "MasterBlock.h"
#include <cstring>

MasterBlock::MasterBlock() {
    id = 0;
    type = Datablock::master;
    root = 1;
}

MasterBlock::MasterBlock(char * input) {
    id = 0;
    type = Datablock::master;
    root = 1;
    loadSerialized(input);
}

void MasterBlock::print() {
    std::cout << "Datablock " << id << ": Master Datablock" << std::endl;
    std::cout << "Index Root: " << root << std::endl;
    std::cout << "Table data: " << std::endl;
    int i=0;
    for (auto d : datablocks) {
        std::cout << ++i << ": " << d << std::endl;
    }
}

char * MasterBlock::getSerialized() {
    char *data = (char*)calloc(1, Datablock::size);
    int pointer = 0;
    uint16_t size = datablocks.size();
    unsigned char type = Datablock::master;
    memcpy(data+pointer, &type, 1);
    pointer+=2;
    memcpy(data+pointer, &root, 2);
    pointer+=2;
    memcpy(data+pointer, &size, 2);
    for (auto d : datablocks) {
        pointer+=2;
        memcpy(data+pointer, &d, 2);
    }
    return data;
}

void MasterBlock::loadSerialized(char *input) {
    int pointer = 0;
    pointer+=2;
    memcpy(&root, input+pointer, 2);
    pointer+=2;
    uint16_t size;
    memcpy(&size, input+pointer, 2);
    for (int i = 0; i < size; ++i) {
        pointer+=2;
        int block;
        memcpy(&block, input+pointer, 2);
        datablocks.insert(block);
    }
}
