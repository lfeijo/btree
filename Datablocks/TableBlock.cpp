//
// Created by Lucas Feijo on 5/12/16.
//

#include <cstdlib>
#include <cmath>
#include <cstring>
#include "TableBlock.h"
#include "../DatablockLoader.h"
#include <cstring>

#define subblock_size 16
#define header_total_len 240
#define header_entries 113

TableBlock::TableBlock(int id) {
    this->id = id;
    this->type = Datablock::data;
    for (int i = 0; i < header_entries; ++i) {
        data.push_back(std::pair<uint32_t, std::string>(0, ""));
        header.push_back(std::pair<uint8_t, uint8_t>(0, 0));
    }

    //experimento
    DatablockLoader::masterBlock->datablocks.insert(id);
}

TableBlock::TableBlock(int id, char * input) {
    this->id = id;
    this->type = Datablock::data;
    for (int i = 0; i < header_entries; ++i) {
        data.push_back(std::pair<uint32_t, std::string>(0, ""));
        header.push_back(std::pair<uint8_t, uint8_t>(0, 0));
    }
    loadSerialized(input);

    //experimento
    DatablockLoader::masterBlock->datablocks.insert(id);
}

void TableBlock::print() {

}

void TableBlock::loadSerialized(char * input) {
    int pointer = 14;
    for (int i = 0; i < header_entries; ++i) {
        uint8_t offset, ssize;
        memcpy(&offset, input+pointer, 1);
        pointer+=1;
        memcpy(&ssize, input+pointer, 1);
        pointer+=1;
        header.push_back(std::pair<uint8_t, uint8_t>(offset, ssize));
    }
    pointer = header_total_len; // area de registros
    uint8_t subblock = 200; // inexists
    std::string dataS = "";
    uint32_t pk = 0;
    char * temp;
    for (int k = 0; k < header.size(); ++k) {
        if (subblock != header[k].first) { // new data entry
            if (pk!=0) { // achou outro registro, salvar o anterior
                data.push_back(std::pair<uint32_t, std::string>(pk, dataS));
                int blockSizeCount = ceil(((float)dataS.size()+4.0)/(float)subblock_size);
                for (int i = 0; i < blockSizeCount-1; ++i) {
                    data.push_back(std::pair<uint32_t, std::string>(0, ""));
                }
            }
            memcpy(&pk, input+pointer, 4);
            dataS.assign(input+pointer+4, 12);
        } else { // continuacao do bloco anterior
            dataS.append(input+pointer, 16);
        }
        subblock = header[k].first;
        pointer += subblock_size;
    }
}

char * TableBlock::getSerialized() {
    char *datas = (char*)calloc(1, Datablock::size);
    int pointer = 0;
    memcpy(datas+pointer, &type, 1);
    pointer+=14;
    for (int i = 0; i < header_entries; ++i) {
        memcpy(datas+pointer, &(header[i].first), 1);
        pointer+=1;
        memcpy(datas+pointer, &(header[i].second), 1);
        pointer+=1;
    }
    for (int j = 0; j < data.size(); ++j) {
        if (data[j].first!=0) { // valid entry
            memcpy(datas+pointer, &(data[j].first), 4);
            int blockSizeCount = ceil(((float)data[j].second.size()+4.0)/(float)subblock_size);
            int stringPointer = 0;
            for (int i = 0; i < blockSizeCount; ++i) {
                if (i==0) {
                    memcpy(datas+pointer+4, data[j].second.substr(stringPointer, 12).c_str(), 12);
                    stringPointer +=12;
                } else {
                    memcpy(datas+pointer, data[j].second.substr(stringPointer, 16).c_str(), 16);
                    stringPointer +=16;
                }
                pointer+=16;
            }
        }
    }
    return datas;
}

std::vector<std::tuple<uint32_t, uint32_t, std::string>> TableBlock::findEntries(std::string value) {
    std::vector<std::tuple<uint32_t, uint32_t, std::string>> results;
    for (int i = 0; i < data.size(); ++i) {
        if (data[i].second.find(value) != std::string::npos)
            results.push_back(std::tuple<uint32_t, uint32_t, std::string>(Datablock::rowIDWith(this->id, i), data[i].first, data[i].second));
    }
    return results;
}

int TableBlock::addEntry(uint32_t key, std::string value) {
    int blockSizeCount = ceil(((float)value.size()+4.0)/(float)subblock_size);
    if (blockSizeCount>13) return -1; // nao cabe num registro
    int fit = getFit(blockSizeCount);
    if (fit<0) return -1; // nao cabe nesse datablock
    int offset = (header_total_len/subblock_size)+(fit);
    for (int i = fit; i < fit+blockSizeCount; ++i) {
        header[i].first = offset;
        header[i].second = value.size();
    }
    data[fit].first = key;
    data[fit].second = value;
    return fit;
}

bool TableBlock::removeEntry(uint32_t rowid) {
    header[Datablock::offsetOfRowID(rowid)].first = 0;
    header[Datablock::offsetOfRowID(rowid)].second = 0;
}

// retorna posicao no header onde cabe uma quantidade size de entradas em sequencia
// retorna -1 se nao cabe
int TableBlock::getFit(int blockCount) {
    int curLargestSize = 0;
    for (int i = 0; i < header.size(); ++i)
    {
        if (header[i].first==0) curLargestSize++;
        else curLargestSize = 0;
        if (curLargestSize == blockCount) return i-blockCount+1;
    }
    return -1;
}

bool TableBlock::doesFit(int sizeBytes) {
    return getFit(ceil(((float)size+4.0)/(float)subblock_size))!=-1 && size<=204;
}

std::string TableBlock::getRegistry(int rowid)
{
    std::pair<uint8_t, uint8_t>headerEntry = header[rowid];
    return data[rowid].second;
}

