//
// Created by Lucas Feijo on 5/12/16.
//

#include "BBranchBlock.h"
#include "../DatablockLoader.h"
#include <cstdlib>
#include <iostream>

BBranchBlock::BBranchBlock(int id) {
    this->id = id;
    this->type = Datablock::branch;

    //experimento
    DatablockLoader::masterBlock->datablocks.insert(id);
}

BBranchBlock::BBranchBlock(int id, char * input) {
    this->id = id;
    this->type = Datablock::branch;
    loadSerialized(input);

    //experimento
    DatablockLoader::masterBlock->datablocks.insert(id);
}

void BBranchBlock::print() {

}

void BBranchBlock::loadSerialized(char * input) {
    int pointer = 0, size = 0;
    pointer+=1;
    memcpy(&size, input+pointer, 1);
    pointer+=3;
    pointer+=8; // reserved
    uint16_t poin;
    uint32_t key;
    for (int i = 0; i < size; ++i) {
        memcpy(&poin, input+pointer, 4);
        pointer += 2;
        pointer += 2; //free padding
        memcpy(&key, input+pointer, 4);
        pointers.push_back(poin);
        keys.push_back(key);
        pointer+=4;
    }
    memcpy(&poin, input+pointer, 4);
    pointers.push_back(poin);
}

char * BBranchBlock::getSerialized() {
    char *data = (char*)calloc(1, Datablock::size);
    int pointer = 0;
    uint16_t padding = 0;
    unsigned char size = keys.size();
    unsigned char type = Datablock::branch;
    memcpy(data+pointer, &type, 1);
    pointer+=1;
    memcpy(data+pointer, &size, 1);
    pointer+=3;
    pointer+=8; // reserved
    for (int i = 0; i < BTREE_D*2; ++i) {
        if (keys.size() > i) {
            memcpy(data + pointer, &pointers[i], 2);
            pointer+=2;
            memcpy(data + pointer, &padding, 2);
            pointer += 2;
            memcpy(data + pointer, &keys[i], 4);
            pointer += 4;
        }
        else
        {
            memcpy(data + pointer, &pointers[i], 2);
            pointer+=2;
            memcpy(data + pointer, &padding, 2);
            break;
        }
    }
    return data;
}

void BBranchBlock::setKeys(std::vector<uint32_t> newKeys)
{
    keys = newKeys;
}

void BBranchBlock::setPointers(std::vector<uint16_t> newPointers)
{
    pointers = newPointers;
}

void BBranchBlock::addEntry(uint16_t pointerL, uint32_t key, uint16_t pointerR)
{
    if(keys.size() == 0)
    {
        keys.push_back(key);
        pointers.push_back(pointerL);
        pointers.push_back(pointerR);
        return;
    }
    bool found = false;
    int k = 0;
    for(int i = 0; i < keys.size(); i++)
    {
        if(found)
            break;
        if(key <= keys[i])
        {
            found = true;
            k = i;
            uint32_t auxKey;
            for(int j = i; j < keys.size(); ++j)
            {
                auxKey = keys[j];
                keys[j] = key;
                key = auxKey;
            }
        }
    }
    keys.push_back(key);

    if(!found)
    {
        pointers.push_back(pointerR);
    }
    else if(k == 0)
    {
        for(int j = k+1; j < pointers.size(); j++)
        {
            uint16_t auxPtr = pointers[j];
            pointers[j] = pointerR;
            pointerR = auxPtr;
        }
        pointers.push_back(pointerR);
    }
    else
    {
        for(int j = k+1; j < pointers.size(); j++)
        {
            uint16_t auxPtr = pointers[j];
            pointers[j] = pointerR;
            pointerR = auxPtr;
        }
        pointers.push_back(pointerR);
    }


}

std::vector<uint16_t> * BBranchBlock::getPointers()
{
    return &pointers;
}

std::vector<uint32_t> * BBranchBlock::getKeys()
{
    return &keys;
}

