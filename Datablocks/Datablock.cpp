//
// Created by Lucas Feijo on 5/11/16.
//

#include "Datablock.h"

uint32_t Datablock::rowIDWith(uint16_t id, uint16_t offset) {
    uint32_t rowid = 0;
    uint32_t idLarge = id << 16;
    rowid |= idLarge;
    rowid |= offset;
    return rowid;
}

uint16_t Datablock::idOfRowID(uint32_t rowid) {
    uint16_t id = 0;
    id = rowid & 0xFFFF0000;
    id = id >> 16;
    return id;
}

uint16_t Datablock::offsetOfRowID(uint32_t rowid) {
    uint16_t offset = 0;
    offset = rowid & 0x0000FFFF;
    return offset;
}





