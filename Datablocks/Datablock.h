//
// Created by Lucas Feijo on 5/11/16.
//

#ifndef V1_DATABLOCK_H
#define V1_DATABLOCK_H


#include <cstdint>
#include <string>

class Datablock {
public:

    // tamanho dos datablocks eh 2KB
    static const int size = 2048;
    // excluindo o datablock de controle zero
    static const int quant = 16383;
    // datablock types
    enum BlockTypes { empty = 0, master = 1, data = 2, leaf = 3, branch = 4 };

    static uint32_t rowIDWith(uint16_t id, uint16_t offset);
    static uint16_t idOfRowID(uint32_t rowid);
    static uint16_t offsetOfRowID(uint32_t rowid);

    uint16_t id;
    uint8_t type;
    virtual void print() = 0;
    virtual char * getSerialized() = 0;
    virtual void loadSerialized(char *input) = 0;
};


#endif //V1_DATABLOCK_H
