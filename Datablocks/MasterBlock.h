//
// Created by Lucas Feijo on 5/15/16.
//

#ifndef V1_CONTROLBLOCK_H
#define V1_CONTROLBLOCK_H


#include <vector>
#include <set>
#include "Datablock.h"

class MasterBlock : public Datablock {

public:
    MasterBlock();
    MasterBlock(char * input);

    // o numero do datablock raiz do indice externo b*tree
    uint16_t root;
    // numero dos datablocks que formam os blocos de dados da tabela
    std::set<uint16_t> datablocks;

    virtual void print();
    virtual char * getSerialized();
    virtual void loadSerialized(char *input);

private:


};


#endif //V1_CONTROLBLOCK_H
