//
// Created by Lucas Feijo on 5/12/16.
//

#include <cstdlib>
#include <iostream>
#include "BLeafBlock.h"
#include "../DatablockLoader.h"
#include <cstring>

BLeafBlock::BLeafBlock(int id) {
    this->id = id;
    this->type = Datablock::leaf;

    //experimento
    DatablockLoader::masterBlock->datablocks.insert(id);
}

BLeafBlock::BLeafBlock(int id, char * data) {
    this->id = id;
    this->type = Datablock::leaf;
    loadSerialized(data);

    //experimento
    DatablockLoader::masterBlock->datablocks.insert(id);
}

void BLeafBlock::print() {
    std::cout << "Datablock " << id << ": B*tree Leaf" << std::endl;
    std::cout << "Left: " << sibblingLeft << std::endl;
    std::cout << "Right: " << sibblingRight << std::endl;
    for (int i = 0; i < elements.size(); i++) {
        std::cout << i << ": PK " << elements[i].first << " -> RowID " << elements[i].second << std::endl;
    }
}

char * BLeafBlock::getSerialized() {
    char *data = (char*)calloc(1, Datablock::size);
    int pointer = 0;
    unsigned char size = elements.size();
    unsigned char type = Datablock::leaf;
    memcpy(data+pointer, &type, 1);
    pointer+=1;
    memcpy(data+pointer, &size, 1);
    pointer+=3;
    memcpy(data+pointer, &sibblingLeft, 2);
    pointer+=2;
    memcpy(data+pointer, &sibblingRight, 2);
    pointer+=2;
    pointer+=8; // reserved
    for (int i = 0; i < BTREE_D*2; ++i) {
        if (elements.size() > i) {
            memcpy(data + pointer, &elements[i].first, 4);
            memcpy(data + pointer + 4, &elements[i].second, 4);
            pointer += 8;
        } else break;
    }
    return data;
}

void BLeafBlock::loadSerialized(char *input) {
    int pointer = 0, size = 0;
    pointer+=1;
    memcpy(&size, input+pointer, 1);
    pointer+=3;
    memcpy(&sibblingLeft, input+pointer, 2);
    pointer+=2;
    memcpy(&sibblingRight, input+pointer, 2);
    pointer+=2;
    pointer+=8; // reserved
    for (int i = 0; i < size; ++i) {
        int key, rowid;
        memcpy(&key, input+pointer, 4);
        memcpy(&rowid, input+pointer+4, 4);
        elements.push_back(std::pair<uint32_t, uint32_t>(key, rowid));
        pointer+=8;
    }
}

uint16_t BLeafBlock::getSibblingLeft() {
    return sibblingLeft;
}

void BLeafBlock::setSibblingLeft(uint16_t sibblingLeft) {
    if (sibblingLeft > 0 && sibblingLeft < Datablock::quant)
        BLeafBlock::sibblingLeft = sibblingLeft;
}

uint16_t BLeafBlock::getSibblingRight() {
    return sibblingRight;
}

void BLeafBlock::setSibblingRight(uint16_t sibblingRight) {
    if (sibblingRight > 0 && sibblingRight < Datablock::quant)
        BLeafBlock::sibblingRight = sibblingRight;
}

void BLeafBlock::addEntry(uint32_t key, uint32_t rowid) {
    if (elements.size()<BTREE_D*2) {
        bool found = false;
        std::pair<uint32_t, uint32_t> newPair = std::make_pair(key, rowid);
        for(int i = 0; i < elements.size(); i++)
        {
            if(found)
                break;
            if(key <= elements[i].first)
            {
                found = true;
                std::pair<uint32_t, uint32_t> auxPair;
                for(int j = i; j < elements.size(); ++j)
                {
                    auxPair = elements[j];
                    elements[j] = newPair;
                    newPair = auxPair;
                }
            }
        }
        elements.push_back(newPair);

    } else {
        printf("ERR Impossible to add %d, block %d full.\n", key, id);
    }
}

void BLeafBlock::removeEntry(int entryIndex)
{
    std::vector<std::pair<uint32_t, uint32_t>> * newElements = getElements();
    newElements->erase(newElements->begin()+entryIndex);
    setElements(*newElements);
}

bool BLeafBlock::hasKey(uint32_t key) {
    for (int i = 0; i < elements.size(); ++i) {
        if (elements[i].first == key) return true;
    } return false;
}

std::vector<std::pair<uint32_t, uint32_t>> *BLeafBlock::getElements()
{
    return &elements;
}

void BLeafBlock::setElements(std::vector<std::pair<uint32_t, uint32_t>> newElements)
{
    elements = newElements;
}