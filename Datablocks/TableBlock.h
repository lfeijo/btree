//
// Created by Lucas Feijo on 5/12/16.
//

#ifndef V1_TABLEBLOCK_H
#define V1_TABLEBLOCK_H


#include <string>
#include <vector>
#include "Datablock.h"

class TableBlock : public Datablock {

public:
    TableBlock(int id);
    TableBlock(int id, char * input);

    // @return -1 se nao couber entrada, id se inserido com sucesso
    int addEntry(uint32_t key, std::string value);

    // @return true se removeu com sucesso, false caso contrario
    bool removeEntry(uint32_t rowid);

    // @return lista de tuplas (rowid, pk, valor) que contem o dado valor
    std::vector<std::tuple<uint32_t, uint32_t, std::string>> findEntries(std::string value);

    // @param size Tamanho total do valor registro, nao incluindo o tamanho da PK
    // @return se cabem 'size' bytes no datablock
    bool doesFit(int sizeBytes);

    // @param blockCount Quantidade de sub-blocos
    // @return -1 caso nao caiba
    // @return int!=-1 posicao no header
    int getFit(int blockCount);

    std::string getRegistry(int rowid);

    virtual void print();
    virtual char * getSerialized();
    virtual void loadSerialized(char * input);

private:

    // header formado por:
    //  distancia em blocos de 16B do inicio do datablock
    //  tamanho do registro em bytes
    std::vector<std::pair<uint8_t, uint8_t>> header;

    // cada entrada da area de dados pode ter de 16B a 208B, variando em sub-blocos de 16B.
    std::vector<std::pair<uint32_t, std::string>> data;
};


#endif //V1_TABLEBLOCK_H
