//
// Created by Lucas Feijo on 5/12/16.
//

#ifndef V1_BBRANCH_H
#define V1_BBRANCH_H

#include <string>
#include <vector>
#include "Datablock.h"

class BBranchBlock : public Datablock {

public:
    BBranchBlock(int id);
    BBranchBlock(int id, char * input);

    virtual void print();
    virtual char * getSerialized();
    virtual void loadSerialized(char * input);

    std::vector<uint16_t> *getPointers();
    std::vector<uint32_t> *getKeys();

    void setKeys(std::vector<uint32_t> newKeys);
    void setPointers(std::vector<uint16_t> newPointers);

    void addEntry(uint16_t pointerL, uint32_t key, uint16_t pointerR);

private:
    std::vector<uint16_t> pointers;
    std::vector<uint32_t> keys;
};

#endif //V1_BBRANCH_H
