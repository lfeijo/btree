//
// Created by Lucas Feijo on 5/12/16.
//

#ifndef V1_BLEAF_H
#define V1_BLEAF_H

#include <string>
#include <vector>
#include "Datablock.h"

// Capacidade datablock 2048B
// Tamanho elemento 4B chave + 4B rowid
// Capacidade de elementos 250 (256 - 6 header)

#define BTREE_D 127 // metade da capacidade de registros

class BLeafBlock : public Datablock {

public:
    // construtor recebe numero do datablock
    BLeafBlock(int id);
    BLeafBlock(int id, char * data);

    virtual void print();
    virtual char * getSerialized();
    virtual void loadSerialized(char *input);

    uint16_t getSibblingLeft();
    void setSibblingLeft(uint16_t sibblingLeft);
    uint16_t getSibblingRight();
    void setSibblingRight(uint16_t sibblingRight);

    void addEntry(uint32_t key, uint32_t rowid);
    void removeEntry(int entryIndex);
    bool hasKey(uint32_t key);

    std::vector<std::pair<uint32_t, uint32_t>> *getElements();
    void setElements(std::vector<std::pair<uint32_t, uint32_t>> elements);


private:
    // numero dos datablocks a direita e esquerda desse
    // zero caso nao exista
    uint16_t sibblingLeft  = 0;
    uint16_t sibblingRight = 0;

    std::vector<std::pair<uint32_t, uint32_t>> elements;

    // vetor de registros
    // cada registro: PK -> RowID
    // RowID -> DatablockID RegistroIndex


};

#endif //V1_BLEAF_H
