//
// Created by Lucas Feijo on 5/20/16.
//

#include <regex>
#include <sstream>
#include <utility>
#include <string>
#include <algorithm>
#include "CommandParser.h"

Command CommandParser::getCommand(string line) {
    vector<string> words = explode(line, ' ');
    Command cmdObj;
    if (words.size()>1) {

        if (toLower(words[0]).compare(Command::SELECT) == 0) {
            cmdObj.command = Command::SELECT;

            if (toLower(words[1]).compare(Command::LIKE) == 0) { // sequential text search
                if (words.size()>2) {
                    cmdObj.value = line.substr(getEndOfWord(line, words[1])+1, line.size()-getEndOfWord(line, words[1]));
                } else {
                    cmdObj.error = true;
                    cmdObj.errorType = Command::ERROR_EXPECTED_ARGUMENT;
                    cmdObj.errorPosition = getEndOfWord(line, words[1]);
                }

            } else if (toLower(words[1]).compare(Command::KEY) == 0) { // expects key, index search
                if (isNumber(words[2])) {
                    cmdObj.key = stoi(words[2]);
                } else {
                    cmdObj.error = true;
                    cmdObj.errorType = Command::ERROR_TOKEN_INVALID;
                    cmdObj.errorPosition = getStartOfWord(line, words[2]);
                }
            } else {
                cmdObj.error = true;
                cmdObj.errorType = Command::ERROR_TOKEN_INVALID;
                cmdObj.errorPosition = getStartOfWord(line, words[1]);
            }


        } else if (toLower(words[0]).compare(Command::INSERT) == 0) {
            cmdObj.command = Command::INSERT;
            if (isNumber(words[1])) {
                cmdObj.key = stoi(words[1]);
                if (words.size()>2) {
                    cmdObj.value = line.substr(getEndOfWord(line, words[1])+1, line.size()-getEndOfWord(line, words[1]));
                } /*else {
                    cmdObj.error = true;
                    cmdObj.errorType = Command::ERROR_EXPECTED_ARGUMENT;
                    cmdObj.errorPosition = getEndOfWord(line, words[1]);
                }*/
            } else {
                cmdObj.error = true;
                cmdObj.errorType = Command::ERROR_TOKEN_INVALID;
                cmdObj.errorPosition = getStartOfWord(line, words[1]);
            }


        } else if (toLower(words[0]).compare(Command::UPDATE) == 0) {
            cmdObj.command = Command::UPDATE;

            if (isNumber(words[1])) {
                cmdObj.key = stoi(words[1]);
                if (words.size()>2) {
                    cmdObj.value = line.substr(getEndOfWord(line, words[1])+1, line.size()-getEndOfWord(line, words[1]));
                } else {
                    cmdObj.error = true;
                    cmdObj.errorType = Command::ERROR_EXPECTED_ARGUMENT;
                    cmdObj.errorPosition = getEndOfWord(line, words[1]);
                }
            } else {
                cmdObj.error = true;
                cmdObj.errorType = Command::ERROR_TOKEN_INVALID;
                cmdObj.errorPosition = getStartOfWord(line, words[1]);
            }


        } else if (toLower(words[0]).compare(Command::DELETE) == 0) {
            cmdObj.command = Command::DELETE;

            if (isNumber(words[1])) {
                cmdObj.key = stoi(words[1]);
            } else {
                cmdObj.error = true;
                cmdObj.errorType = Command::ERROR_TOKEN_INVALID;
                cmdObj.errorPosition = getStartOfWord(line, words[1]);
            }

        } else {
            cmdObj.error = true;
            cmdObj.errorType = Command::ERROR_TOKEN_INVALID;
            cmdObj.errorPosition = getStartOfWord(line, words[0]);
        }
    } else {
        if (words.size()>0) {
            if (words.size() == 1 && toLower(words[0]).compare(Command::COMMIT) == 0) {
                cmdObj.command = Command::COMMIT;
            } else {
                if (toLower(words[0]).compare(Command::SELECT) == 0 ||
                    toLower(words[0]).compare(Command::UPDATE) == 0 ||
                    toLower(words[0]).compare(Command::INSERT) == 0 ||
                    toLower(words[0]).compare(Command::DELETE) == 0) {
                    cmdObj.error = true;
                    cmdObj.errorType = Command::ERROR_EXPECTED_ARGUMENT;
                    cmdObj.errorPosition = getEndOfWord(line, words[0]);
                } else {
                    cmdObj.error = true;
                    cmdObj.errorType = Command::ERROR_TOKEN_INVALID;
                    cmdObj.errorPosition = getStartOfWord(line, words[0]);
                }
            }
        } else {
            cmdObj.command = Command::EMPTY;
        }
    }
    return cmdObj;
}


std::vector<std::string> CommandParser::explode(std::string const & s, char delim) {
    std::vector<std::string> result;
    std::istringstream iss(s);
    for (std::string token; std::getline(iss, token, delim); )
    {
        if (!token.empty()) {
            result.push_back(std::move(token));
        }
    }
    return result;
}

bool CommandParser::isNumber(std::string s) {
    return !s.empty() && s.find_first_not_of("0123456789") == std::string::npos;
}

int CommandParser::getEndOfWord(string line, string word) {
    return line.find(word)+word.size();
}

int CommandParser::getStartOfWord(string line, string word) {
    return line.find(word);
}

string CommandParser::toLower(string data) {
    std::transform(data.begin(), data.end(), data.begin(), ::tolower);
    return data;
}








