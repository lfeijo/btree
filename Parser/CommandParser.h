//
// Created by Lucas Feijo on 5/20/16.
//

#ifndef V1_COMMANDPARSER_H
#define V1_COMMANDPARSER_H


#include "Command.h"

class CommandParser {

public:
    static Command getCommand(string command);

private:
    static std::vector<std::string> explode(std::string const & s, char delim);
    static bool isNumber(string val);
    static int getEndOfWord(string line, string word);
    static int getStartOfWord(string line, string word);

    static string toLower(string word);
};


#endif //V1_COMMANDPARSER_H
