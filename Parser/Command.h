//
// Created by Lucas Feijo on 5/20/16.
//

#ifndef V1_COMMAND_H
#define V1_COMMAND_H

#include <string>

using namespace std;

class Command {

public:

    constexpr static auto SELECT = "select";
    constexpr static auto UPDATE = "update";
    constexpr static auto INSERT = "insert";
    constexpr static auto DELETE = "delete";
    constexpr static auto COMMIT = "commit";
    constexpr static auto EMPTY = "empty";

    constexpr static auto LIKE = "like";
    constexpr static auto KEY = "key";

    constexpr static auto ERROR_TOKEN_INVALID = "invalid token";
    constexpr static auto ERROR_EXPECTED_ARGUMENT = "expected argument";

    Command();
    Command(string, int, string);

    string command;
    int key = 0;
    string value;

    bool error = false;
    string errorType;
    int errorPosition = 0;
};


#endif //V1_COMMAND_H
