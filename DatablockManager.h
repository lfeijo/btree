//
// Created by Lucas Feijo on 5/23/16.
//

#ifndef V1_DATABLOCKMANAGER_H
#define V1_DATABLOCKMANAGER_H


#include "Datablocks/TableBlock.h"
#include "DatablockLoader.h"
#include "BTree.h"

class DatablockManager {
public:
    // O metodo retorna um TABLE BLOCK onde cabe um registro de tamanho entrySize.
    // Caso nao caiba em nenhum, busca um vazio no datafile
    // Nao eh marcado como utilizado no master block.
    // @return Table Block onde cabe o tamanho de registro passado, NULL caso nao caiba
    static TableBlock *findFit(uint8_t entrySize);

    // Adiciona registro em um table block onde caiba, o salva e retorna id dele.
    // Marca bloco como utilizado no master block.
    // @return id do datablock onde adicionou, zero caso nao seja possivel
    static uint16_t addEntry(uint32_t pk, std::string value);

    // Remove registro em um table block
    // @Return True se removeu com sucesso, false caso contrario
    static bool removeEntry(int index);

    // Performa uma busca sequencial por valor usando o metodo findEntries do table block.
    // @return uma lista de tuplas (rowid, pk, valor)
    static std::vector<std::tuple<uint32_t, uint32_t, std::string>> findByValue(std::string value);

    //Encontra e retorna registro utilizando a busca pelo indice
    static std::string findByIndex(int index);

    static int getUniqueId();

private:
    int static idCounter;
};


#endif //V1_DATABLOCKMANAGER_H
