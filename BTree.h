//
// Created by Alonso Ali Goncalves on 5/21/16.
//

#ifndef V1_BTREE_H
#define V1_BTREE_H
#include "Datablocks/MasterBlock.h"
#include "Datablocks/BBranchBlock.h"
#include "DatablockLoader.h"

class BTree
{
public:
    BTree();
    std::string findRegistry(int id);
    void printTree();
    void addEntry(uint32_t key, uint32_t rowid);
    bool removeEntry(int searchid);


private:
    Datablock * root;

    std::pair<uint32_t,Datablock *> addEntryAux(uint32_t key, uint32_t rowid, Datablock * currentDatablock);

    void auxPrintTree(Datablock * currentDatablock);

    void updateTree();
    void updateTreeAux(Datablock * datablock);

    void checkAndUpdateFathersMinimum(int searchid);
};

#endif //V1_BTREE_H
