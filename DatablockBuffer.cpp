//
// Created by Lucas Feijo on 5/15/16.
//

#include "DatablockBuffer.h"
#include "DatablockLoader.h"

Datablock * DatablockBuffer::getDatablock(int id) {
    if (getDatablockBuffer()->size()>0) {
        for (std::list<Datablock *>::const_iterator iterator = getDatablockBuffer()->begin(), end = getDatablockBuffer()->end(); iterator != end; ++iterator) {
            if ((*iterator) != NULL) {
                if ((*iterator)->id == id) {
                    return (Datablock *) *iterator; // derreferencia e retorna
                }
            }
        }
    }
    return NULL; // nao achou
}

void DatablockBuffer::storeDatablock(Datablock * datablock) {
    if (getDatablockBuffer()->size() == maxSize) { // remove e insere
        DatablockLoader::persistDatablock(getDatablockBuffer()->front()); // WRITE BACK CACHE: salvando logo antes de deletar do buffer
        getDatablockBuffer()->pop_front(); // remove da frente
    }
    getDatablockBuffer()->push_back(datablock); // insere atras
}

bool DatablockBuffer::flush() {
    bool success = true;
    if (getDatablockBuffer()->size()>0) {
        for (std::list<Datablock *>::const_iterator iterator = getDatablockBuffer()->begin(), end = getDatablockBuffer()->end(); iterator != end; ++iterator) {
            if ((*iterator) != NULL) {
                success &= DatablockLoader::persistDatablock((Datablock *) *iterator);
            }
        }
    }
    return success;
}

