//
// Created by Alonso Ali Goncalves on 5/21/16.
//

#include "BTree.h"
#include "Datablocks/TableBlock.h"
#include "DatablockManager.h"
#include <cstdlib>

#define TESTSIZE 9

#define REGCAP 254


BTree::BTree()
{
    int rootID = DatablockLoader::masterBlock->root;
    if(rootID == 1)
    {
        root = new BLeafBlock(DatablockManager::getUniqueId());
        DatablockLoader::masterBlock->root = root->id;
        DatablockLoader::saveDatablock(root);
    }
    else
    {
        root = DatablockLoader::loadDatablock(rootID);
    }
}



void BTree::printTree()
{
    auxPrintTree(root);
}

void BTree::auxPrintTree(Datablock * currentDatablock)
{
    if(currentDatablock->type == Datablock::leaf)
    {
        //eh leaf
        BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;
        printf("Leaf %i:[ ", leafBlock->id);
        for(int i = 0; i < leafBlock->getElements()->size(); i++)
        {
            printf("%i ", (*leafBlock->getElements())[i].first);
        }
        printf("]");

        printf(" L: %i ", leafBlock->getSibblingLeft());
        printf("R: %i\n", leafBlock->getSibblingRight());
    }
    else
    {
        BBranchBlock * branchBlock = (BBranchBlock *)currentDatablock;
        printf("Branch %i:[ ", branchBlock->id);
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            printf("%i ", (*branchBlock->getKeys())[i]);
        }
        printf("]\n");

        for(int i = 0; i < branchBlock->getPointers()->size(); i++)
        {
            Datablock * meme = DatablockLoader::loadDatablock((*branchBlock->getPointers())[i]);
            auxPrintTree(meme);
        }
    }
}

uint32_t fetchMinValue(Datablock *datablock)
{
    if(datablock->type == Datablock::leaf)
    {
        BLeafBlock * leafBlock = (BLeafBlock *)datablock;
        return leafBlock->getElements()->at(0).first;
    }

    BBranchBlock * branchBlock = (BBranchBlock *)datablock;
    return fetchMinValue(DatablockLoader::loadDatablock(branchBlock->getPointers()->at(0)));
}

void BTree::updateTreeAux(Datablock * datablock)
{
    if(datablock->type == Datablock::leaf)
    {
        return;
    }
    BBranchBlock * branchBlock = (BBranchBlock *)datablock;
    for(int i = 0; i < branchBlock->getKeys()->size(); i++)
    {
        (*branchBlock->getKeys())[i] = fetchMinValue(DatablockLoader::loadDatablock(branchBlock->getPointers()->at(i+1)));
    }

    for(int i = 0; i < branchBlock->getPointers()->size(); i++)
    {
        updateTreeAux(DatablockLoader::loadDatablock(branchBlock->getPointers()->at(i)));
    }
}

void BTree::updateTree()
{
    updateTreeAux(root);
}



/*BBranchBlock * updateFather(int searchid)
{
    BBranchBlock * fatherDatablock;
    unsigned long fatherIndex = 0;

    bool found = false;

    BLeafBlock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        fatherDatablock = branchBlock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            fatherIndex = i;
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if (pointer != fatherDatablock->getPointers()->at(0))
            fatherIndex--;

        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }



    if(fatherDatablock == nullptr)
    {
        printf("Datablock Pai nulo! origem: update Father");
        return nullptr;
    }

    //Fim da busca
    //Neste ponto temos a garantia de que o datablock que temos em mao eh um do tipo FOLHA
    BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;



    //Atualiza pai
    std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
    fatherDatablockNew->at(fatherIndex) = leafBlock->getElements()->front().first;
    fatherDatablock->setKeys(*fatherDatablockNew);

    return fatherDatablock;
}*/

std::vector<std::vector<std::pair<uint32_t ,uint32_t >>> doubleSplit(BLeafBlock * leaf1, BLeafBlock * leaf2, std::pair<uint32_t,uint32_t >pair)
{
    //recebemos dois datablock e o par de entrada novo
    //juntamos todas as entradas e a nova em um unico vetor e depois dividimos 1/3 para cada (incluindo o datablock novo)
    std::vector<std::pair<uint32_t ,uint32_t>> allElements;

    bool pairInserted = false;

    for(std::vector<std::pair<uint32_t ,uint32_t>>::iterator it = leaf1->getElements()->begin(); it < leaf1->getElements()->end(); it++)
    {
        if(!pairInserted && (*it).first > pair.first)
        {
            allElements.push_back(pair);
            pairInserted = true;
        }
        allElements.push_back(*it);
    }

    for(std::vector<std::pair<uint32_t ,uint32_t>>::iterator it = leaf2->getElements()->begin(); it < leaf2->getElements()->end(); it++)
    {
        if(!pairInserted && (*it).first > pair.first)
        {
            allElements.push_back(pair);
            pairInserted = true;
        }
        allElements.push_back(*it);
    }

    if(!pairInserted)
        allElements.push_back(pair);


    std::vector<std::pair<uint32_t ,uint32_t>> firstThird;
    std::vector<std::pair<uint32_t ,uint32_t>> secondThird;
    std::vector<std::pair<uint32_t ,uint32_t>> lastThird;

    int size = allElements.size();
    int count = 0;
    for(std::vector<std::pair<uint32_t ,uint32_t>>::iterator it = allElements.begin(); it < allElements.end(); it++, count++)
    {
        if(count < size/3)
        {
            firstThird.push_back(*it);
        }
        else if(count < 2 * size/3)
        {
            secondThird.push_back(*it);
        }
        else
        {
            lastThird.push_back(*it);
        }
    }

    std::vector<std::vector<std::pair<uint32_t ,uint32_t >>> lala;
    lala.push_back(firstThird);
    lala.push_back(secondThird);
    lala.push_back(lastThird);

    return lala;
}

void BTree::addEntry(uint32_t key, uint32_t rowid)
{
    std::pair<uint32_t,Datablock *> pair = addEntryAux(key, rowid, root);
    if(pair.second != nullptr)
    {
        BBranchBlock * newRoot = new BBranchBlock(DatablockManager::getUniqueId());
        newRoot->addEntry(root->id, pair.first, pair.second->id);
        DatablockLoader::saveDatablock(newRoot);
        root = newRoot;
        DatablockLoader::masterBlock->root = root->id;
    }

}

std::pair<uint32_t,Datablock *> BTree::addEntryAux(uint32_t key, uint32_t rowid, Datablock * currentDatablock)
{
    if(currentDatablock->type == Datablock::leaf) //caso base da recursao, se chegamos na folha
    {
        BLeafBlock *leafBlock = (BLeafBlock *) currentDatablock;
        //eh leaf
        if (leafBlock->getElements()->size() == TESTSIZE)
        {
            //datablock esta cheio
            //temos que checar os irmaos
            //bool sibFull = false;

            uint16_t leftSibblingId = leafBlock->getSibblingLeft();
            uint16_t rightSibblingId = leafBlock->getSibblingRight();
            if(leftSibblingId != 0)//se existe
            {
                BLeafBlock * leftSibbling = (BLeafBlock *)DatablockLoader::loadDatablock(leftSibblingId);
                std::vector<std::pair<uint32_t, uint32_t>> * leftSibblingElements = leftSibbling->getElements();
                if(leftSibblingElements->size() != TESTSIZE)
                {
                    //irmao da esquerda tem espaco vazio
                    leftSibbling->addEntry((*leafBlock->getElements())[0].first, (*leafBlock->getElements())[0].second);
                    leafBlock->removeEntry(0);
                    leafBlock->addEntry(key, rowid);
                    //updateFather(leafBlock->getElements()->front().first);
                    updateTree();
                    return std::pair<uint32_t , Datablock *>(0, nullptr);
                }
                else
                {
                    uint32_t save = leafBlock->getElements()->front().first;
                    std::pair<uint32_t ,uint32_t > newPair = std::pair<uint32_t ,uint32_t >(key,rowid);
                    std::vector<std::vector<std::pair<uint32_t,uint32_t >>> contents = doubleSplit(leftSibbling, leafBlock, newPair);
                    leftSibbling->setElements(contents[0]);
                    leafBlock->setElements(contents[1]);

                    BLeafBlock * newLeafBlock = new BLeafBlock(DatablockManager::getUniqueId());
                    newLeafBlock->setSibblingLeft(leafBlock->id);
                    newLeafBlock->setSibblingRight(leafBlock->getSibblingRight());

                    if(leafBlock->getSibblingRight() != 0)
                    {
                        ((BLeafBlock *)DatablockLoader::loadDatablock(leafBlock->getSibblingRight()))->setSibblingLeft(newLeafBlock->id);
                    }

                    leafBlock->setSibblingRight(newLeafBlock->id);

                    newLeafBlock->setElements(contents[2]);

                    //updateFather(leftSibbling->getElements()->at(0).first);
                    //updateFather(save);

                    DatablockLoader::saveDatablock(newLeafBlock);

                    updateTree();

                    return std::pair<uint32_t ,Datablock*>(newLeafBlock->getElements()->front().first, newLeafBlock);
                }
            }
            else if(rightSibblingId != 0)//nao existe irmao da esquerda, vejamos se o da direita existe
            {
                BLeafBlock * rightSibbling = (BLeafBlock *)DatablockLoader::loadDatablock(rightSibblingId);
                std::vector<std::pair<uint32_t, uint32_t>> * rightSibblingElements = rightSibbling->getElements();
                if(rightSibblingElements->size() != TESTSIZE)
                {
                    //irmao da direita tem espaco vazio
                    rightSibbling->addEntry((*leafBlock->getElements())[TESTSIZE-1].first, (*leafBlock->getElements())[TESTSIZE-1].second);
                    leafBlock->removeEntry(TESTSIZE-1);
                    leafBlock->addEntry(key, rowid);
                    //updateFather(rightSibbling->getElements()->back().first);
                    updateTree();
                    return std::pair<uint32_t , Datablock *>(0, nullptr);
                }
                else
                {
                    uint32_t save = leafBlock->getElements()->front().first;
                    std::pair<uint32_t ,uint32_t > newPair = std::pair<uint32_t ,uint32_t >(key,rowid);
                    std::vector<std::vector<std::pair<uint32_t,uint32_t >>> contents = doubleSplit(leafBlock, rightSibbling, newPair);
                    leafBlock->setElements(contents[0]);
                    rightSibbling->setElements(contents[2]);

                    BLeafBlock * newLeafBlock = new BLeafBlock(DatablockManager::getUniqueId());
                    newLeafBlock->setSibblingLeft(leafBlock->id);
                    newLeafBlock->setSibblingRight(leafBlock->getSibblingRight());

                    rightSibbling->setSibblingLeft(newLeafBlock->id);

                    leafBlock->setSibblingRight(newLeafBlock->id);

                    newLeafBlock->setElements(contents[1]);

                    //updateFather(rightSibbling->getElements()->at(0).first);
                    //updateFather(save);

                    updateTree();

                    DatablockLoader::saveDatablock(newLeafBlock);

                    return std::pair<uint32_t ,Datablock*>(newLeafBlock->getElements()->front().first, newLeafBlock);
                }
            }
            else
            {
                //temos que fazer o split deste datablock apenas pois ele nao tem irmaos
                std::vector<std::pair<uint32_t , uint32_t >> elements = *leafBlock->getElements();

                bool found = false;
                std::pair<uint32_t, uint32_t> newPair = std::make_pair(key, rowid);
                for(int i = 0; i < elements.size(); i++)
                {
                    if(found)
                        break;
                    if(key <= elements[i].first)
                    {
                        found = true;
                        std::pair<uint32_t, uint32_t> auxPair;
                        for(int j = i; j < elements.size(); ++j)
                        {
                            auxPair = elements[j];
                            elements[j] = newPair;
                            newPair = auxPair;
                        }
                    }
                }
                elements.push_back(newPair);

                //inserimos novo registro, agora vamos separar ao meio
                std::vector<std::pair<uint32_t , uint32_t >> firstHalf;
                std::vector<std::pair<uint32_t , uint32_t >> secondHalf;

                for(int i = 0; i < elements.size(); i++)
                {
                    if(i < elements.size()/2)
                    {
                        firstHalf.push_back(elements[i]);
                    }
                    else
                    {
                        secondHalf.push_back(elements[i]);
                    }
                }
                //neste ponto temos dividido cada um dos elementos dos datablocks folha
                //temos então de configurar os novos ponteiros dos irmãos
                BLeafBlock * newLeafBlock = new BLeafBlock(DatablockManager::getUniqueId());
                newLeafBlock->setSibblingLeft(leafBlock->id);
                newLeafBlock->setSibblingRight(leafBlock->getSibblingRight());

                ((BLeafBlock *)DatablockLoader::loadDatablock(leafBlock->getSibblingRight()))->setSibblingLeft(newLeafBlock->id);

                leafBlock->setSibblingRight(newLeafBlock->id);

                //tendo configurado, vamos agora terminar de atualizar os datablocks
                leafBlock->setElements(firstHalf);
                newLeafBlock->setElements(secondHalf);

                DatablockLoader::saveDatablock(newLeafBlock); //DESCOMENTAR PARA SALVAR DATABLOCK


                return std::pair<uint32_t, Datablock*>(secondHalf[0].first, newLeafBlock);
            }



        }
        else
        {
            //datablock tem espaco vazio
            leafBlock->addEntry(key, rowid);
            return std::pair<uint32_t , Datablock *>(0, nullptr);
        }
    }

    //Datablock nao eh folha, temos de seguir descendo na arvore
    BBranchBlock * branchBlock = (BBranchBlock *)currentDatablock;
    uint16_t pointer = 0;
    uint32_t blockkey = 0;
    for(int i = 0; i < branchBlock->getKeys()->size(); i++)
    {
        blockkey = (*branchBlock->getKeys())[i];
        if(key < blockkey)
        {
            pointer = (*branchBlock->getPointers())[i];
            break;
        }
        if(key >= blockkey)
        {
            continue;
        }
    }
    if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
    {
        pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
    }

    Datablock * bottomDatablock = DatablockLoader::loadDatablock(pointer);
    std::pair<uint32_t,Datablock *> newpair = addEntryAux(key, rowid, bottomDatablock); //chamada recursiva
    Datablock * newDatablock = newpair.second;
    if(newDatablock != nullptr)
    {
        //houve a criacao de um novo datablock, temos de adicionar a nova entrada neste datablock
        if(branchBlock->getKeys()->size() == TESTSIZE)
        {
            //datablock esta cheio
            //temos que separar este datablock em outro
            if(newDatablock->type == Datablock::leaf)
            {
                //novo datablock eh folha
                BLeafBlock * newLeafDatablock = (BLeafBlock *)newDatablock;
                std::vector<uint32_t> allKeys = *branchBlock->getKeys(); //novo vetor para adicionar a entrada antes de dividir
                uint32_t newKey = newpair.first;

                //vamos buscar o local de onde inserir a nova chave
                bool found = false;
                int k = 0; //variavel para identificar a posicao, para ser usada na hora de inserir os ponteiros novos
                for(int i = 0; i < allKeys.size(); i++)
                {
                    if(found)
                        break;
                    if(newKey <= allKeys[i])
                    {
                        found = true;
                        k = i;
                        uint32_t auxKey;
                        for(int j = i; j < allKeys.size(); ++j)
                        {
                            auxKey = allKeys[j];
                            allKeys[j] = newKey;
                            newKey = auxKey;
                        }
                    }
                }
                allKeys.push_back(newKey);

                std::vector<uint16_t> allPointers = *branchBlock->getPointers();
                uint16_t pointerL = bottomDatablock->id;
                uint16_t pointerR = newLeafDatablock->id;

                for(int i = 0; i < allPointers.size(); i++)
                {
                    if(pointerL == allPointers[i])
                    {
                        allPointers.insert(allPointers.begin()+i+1, pointerR);
                        break;
                    }
                    if(pointerR == allPointers[i])
                    {
                        allPointers.insert(allPointers.begin()+i, pointerL);
                        break;
                    }


                }
                /*if(!found)
                {
                    allPointers.push_back(pointerR);
                }
                else if(k == 0)
                {
                    for(int j = k; j < allPointers.size(); j++)
                    {
                        uint16_t auxPtr = allPointers[j];
                        allPointers[j] = pointerL;
                        pointerL = auxPtr;
                    }
                    allPointers.push_back(pointerL);
                }
                else
                {
                    for(int j = k+1; j < allPointers.size(); j++)
                    {
                        uint16_t auxPtr = allPointers[j];
                        allPointers[j] = pointerR;
                        pointerR = auxPtr;
                    }
                    allPointers.push_back(pointerR);
                }*/

                //adicionamos a nova entrada, temos de dividi-las agora


                //Divisao das chaves
                std::vector<uint32_t> firstHalfKeys;
                std::vector<uint32_t> secondHalfKeys;
                uint32_t midKey;
                for(int i = 0; i < allKeys.size(); i++)
                {
                    if(i < allKeys.size()/2)
                    {
                        firstHalfKeys.push_back(allKeys[i]);
                    }
                    else if(i == allKeys.size()/2)
                    {
                        midKey = allKeys[i];
                    }
                    else
                    {
                        secondHalfKeys.push_back(allKeys[i]);
                    }
                }

                std::vector<uint16_t> firstHalfPointers;
                std::vector<uint16_t> secondHalfPointers;
                for(int i = 0; i < allPointers.size(); i++)
                {
                    if(i < (allPointers.size()+1)/2)
                    {
                        firstHalfPointers.push_back(allPointers[i]);
                    }
                    else
                    {
                        secondHalfPointers.push_back(allPointers[i]);
                    }
                }


                BBranchBlock * newBranchBlock = new BBranchBlock(DatablockManager::getUniqueId());

                branchBlock->setKeys(firstHalfKeys);
                branchBlock->setPointers(firstHalfPointers);

                newBranchBlock->setKeys(secondHalfKeys);
                newBranchBlock->setPointers(secondHalfPointers);

                DatablockLoader::saveDatablock(newBranchBlock); //DESCOMENTAR PARA SALVAR DATABLOCK

                return std::pair<uint32_t, Datablock *>(midKey, newBranchBlock);
            }
            else
            {
                //novo datablock eh branch
                BBranchBlock * newBranchblock = (BBranchBlock *)newDatablock;
                std::vector<uint32_t> allKeys = *branchBlock->getKeys(); //novo vetor para adicionar a entrada antes de dividir
                uint32_t newKey = newpair.first;

                //vamos buscar o local de onde inserir a nova chave
                bool found = false;
                int k = 0; //variavel para identificar a posicao, para ser usada na hora de inserir os ponteiros novos
                for(int i = 0; i < allKeys.size(); i++)
                {
                    if(found)
                        break;
                    if(newKey <= allKeys[i])
                    {
                        found = true;
                        k = i;
                        uint32_t auxKey;
                        for(int j = i; j < allKeys.size(); ++j)
                        {
                            auxKey = allKeys[j];
                            allKeys[j] = newKey;
                            newKey = auxKey;
                        }
                    }
                }
                allKeys.push_back(newKey);

                std::vector<uint16_t> allPointers = *branchBlock->getPointers();
                uint16_t pointerL = bottomDatablock->id;
                uint16_t pointerR = newBranchblock->id;

                for(int i = 0; i < allPointers.size(); i++)
                {
                    if(pointerL == allPointers[i])
                    {
                        allPointers.insert(allPointers.begin()+i+1, pointerR);
                        break;
                    }
                    if(pointerR == allPointers[i])
                    {
                        allPointers.insert(allPointers.begin()+i, pointerL);
                        break;
                    }


                }

                /*if(!found)
                {
                    allPointers.push_back(pointerR);
                }
                else if(k == 0)
                {
                    for(int j = k; j < allPointers.size(); j++)
                    {
                        uint16_t auxPtr = allPointers[j];
                        allPointers[j] = pointerL;
                        pointerL = auxPtr;
                    }
                    allPointers.push_back(pointerL);
                }
                else
                {
                    for(int j = k+1; j < allPointers.size(); j++)
                    {
                        uint16_t auxPtr = allPointers[j];
                        allPointers[j] = pointerR;
                        pointerR = auxPtr;
                    }
                    allPointers.push_back(pointerR);
                }*/

                //adicionamos a nova entrada, temos de dividi-las agora


                //Divisao das chaves
                std::vector<uint32_t> firstHalfKeys;
                std::vector<uint32_t> secondHalfKeys;
                uint32_t midKey;
                for(int i = 0; i < allKeys.size(); i++)
                {
                    if(i < allKeys.size()/2)
                    {
                        firstHalfKeys.push_back(allKeys[i]);
                    }
                    else if(i == allKeys.size()/2)
                    {
                        midKey = allKeys[i];
                    }
                    else
                    {
                        secondHalfKeys.push_back(allKeys[i]);
                    }
                }

                std::vector<uint16_t> firstHalfPointers;
                std::vector<uint16_t> secondHalfPointers;
                for(int i = 0; i < allPointers.size(); i++)
                {
                    if(i < (allPointers.size()+1)/2)
                    {
                        firstHalfPointers.push_back(allPointers[i]);
                    }
                    else
                    {
                        secondHalfPointers.push_back(allPointers[i]);
                    }
                }


                BBranchBlock * newBranchBlock = new BBranchBlock(DatablockManager::getUniqueId());

                branchBlock->setKeys(firstHalfKeys);
                branchBlock->setPointers(firstHalfPointers);

                newBranchBlock->setKeys(secondHalfKeys);
                newBranchBlock->setPointers(secondHalfPointers);

                DatablockLoader::saveDatablock(newBranchBlock); //DESCOMENTAR PARA SALVAR DATABLOCK

                return std::pair<uint32_t, Datablock *>(midKey, newBranchBlock);

            }


        }
        else
        {
            //tem espaço neste datablock
            //podemos adicionar a nova entrada
            if(newDatablock->type == Datablock::leaf)
            {
                //o novo datablock eh folha
                BLeafBlock * newLeafBlock = (BLeafBlock *)newDatablock;
                uint32_t newDatablockKey = newpair.first;
                uint16_t newDatablockPtr = newLeafBlock->id;
                branchBlock->addEntry(bottomDatablock->id,newDatablockKey, newDatablockPtr);
            }
            else
            {
                //o novo datablock eh branch
                BBranchBlock * newBranchBlock = (BBranchBlock *)newDatablock;
                uint32_t newDatablockKey = (*newBranchBlock->getKeys())[0];
                uint16_t newDatablockPtr = newBranchBlock->id;

                branchBlock->addEntry(bottomDatablock->id, newDatablockKey, newDatablockPtr);
            }
        }
        return std::pair<uint32_t , Datablock*>(0, nullptr);
    }
    return std::pair<uint32_t, Datablock *>(0, nullptr);
}

BBranchBlock * removeFather(int searchid)
{
    BBranchBlock * fatherDatablock;
    int fatherIndex;

    bool found = false;
    bool lastEntry = false;

    BLeafBlock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        lastEntry = false;
        branchBlock = (BBranchBlock *)currentDatablock;
        fatherDatablock = branchBlock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                fatherIndex = i;
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
            lastEntry = true;
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }


    //Neste ponto temos a garantia de que o datablock que temos em mao eh um do tipo FOLHA
    BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;
    std::pair<uint32_t, uint32_t> * pair;
    for(int i = 0; i < leafBlock->getElements()->size(); i++)
    {
        pair = &((*leafBlock->getElements())[i]);
        if(searchid == pair->first)
        {
            int key = pair->second & 0xffff0000;
            key = key >> 16;
            int rowid = pair->second & 0xffff;
            Datablock * targetDatablock = DatablockLoader::loadDatablock(key);
            if(targetDatablock->type == 2) //data
            {
                found = true;
                break;
            }
            else
            {
                printf("Erro no acesso ao datablock na BTree, esperado DATA e foi recebido outro\n");
                break;
            }
        }
    }

    //Fim da busca

    if(!found)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar remover datablock pai de uma folha. Folha não encontrada.\n");
        return nullptr;
    }

    //Remove ponteiro do pai para o filho
    std::vector<uint16_t> * fatherDatablockPointers = fatherDatablock->getPointers();
    fatherDatablockPointers->erase(fatherDatablockPointers->begin()+fatherIndex);
    fatherDatablock->setPointers(*fatherDatablockPointers);

    //Remove entrada do pai
    if(lastEntry)
    {
        std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
        fatherDatablockNew->erase(fatherDatablockNew->begin()+fatherIndex-1);
        fatherDatablock->setKeys(*fatherDatablockNew);
    }
    else
    {
        std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
        fatherDatablockNew->erase(fatherDatablockNew->begin()+fatherIndex);
        fatherDatablock->setKeys(*fatherDatablockNew);
    }

    return fatherDatablock;
}

BBranchBlock * removeFather(BBranchBlock * branch)
{
    int fatherIndex;
    int searchid = branch->getKeys()->front();
    BBranchBlock * fatherDatablock = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    bool found = false;
    bool lastEntry = false;

    BBranchBlock * root = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        fatherDatablock = branchBlock;
        branchBlock = (BBranchBlock *)currentDatablock;
        if(branchBlock->id == branch->id)
        {
            found = true;
            break;
        }
        lastEntry = false;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            fatherIndex = i;
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
            lastEntry = true;
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }

    //Fim da busca

    if(!found)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar atualizar datablock pai de um branch. Branch não encontrado.");
        return nullptr;
    }

    //Remove entrada do pai
    if(lastEntry)
    {
        std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
        fatherDatablockNew->erase(fatherDatablockNew->begin()+fatherIndex-1);
        fatherDatablock->setKeys(*fatherDatablockNew);
    }
    else
    {
        std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
        fatherDatablockNew->erase(fatherDatablockNew->begin()+fatherIndex);
        fatherDatablock->setKeys(*fatherDatablockNew);
    }

    return fatherDatablock;
}

BBranchBlock * updateFather(int searchid)
{
    BBranchBlock * fatherDatablock;
    unsigned long fatherIndex = 0;

    bool found = false;

    BLeafBlock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        fatherDatablock = branchBlock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            fatherIndex = i;
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }

    if(fatherDatablock == nullptr)
    {
        printf("Datablock Pai nulo! origem: update Father\n");
        return nullptr;
    }

    //Fim da busca
    //Neste ponto temos a garantia de que o datablock que temos em mao eh um do tipo FOLHA
    BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;

    //Atualiza pai
    std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
    fatherDatablockNew->at(fatherIndex) = leafBlock->getElements()->front().first;
    fatherDatablock->setKeys(*fatherDatablockNew);

    return fatherDatablock;
}

BBranchBlock * updateFather(BBranchBlock * branch)
{
    int fatherIndex;
    int searchid = branch->getKeys()->front();
    BBranchBlock * fatherDatablock = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    bool found = false;

    BBranchBlock * root = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        fatherDatablock = branchBlock;
        branchBlock = (BBranchBlock *)currentDatablock;
        if(branchBlock->id == branch->id)
        {
            found = true;
            break;
        }
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            fatherIndex = i;
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }

    //Fim da busca

    if(!found)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar atualizar datablock pai de um branch. Branch não encontrado.\n");
//        return false;
    }

    //Atualiza pai
    std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
    fatherDatablockNew->at(fatherIndex) = branch->getKeys()->front();
    fatherDatablock->setKeys(*fatherDatablockNew);

    return fatherDatablock;
}

void checkAndUpdateEmptyFathersAux(int searchid, int deep)
{
    BBranchBlock * fatherDatablock;
    int fatherIndex;

    BLeafBlock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;
    int layer = deep;

    if(deep == 1)
    {
        //Pai a ser atualizado é a raiz, por tanto, iremos diminuir um nível da árvore
        branchBlock = (BBranchBlock *)currentDatablock;
        DatablockLoader::masterBlock->root = (*branchBlock->getPointers())[0];
    }

    while(true)
    {
        layer--;
        if(layer == 1) //Chegamos no pai a ser atualizado
        {
            break;
        }

        branchBlock = (BBranchBlock *)currentDatablock;
        fatherDatablock = branchBlock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            fatherIndex = i;
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }

    //Remove pai
    std::vector<uint32_t> * fatherDatablockNew = fatherDatablock->getKeys();
    fatherDatablockNew->erase(fatherDatablockNew->begin()+fatherIndex);
    fatherDatablock->setKeys(*fatherDatablockNew);

    if(fatherDatablock->size==0) //Precisa atualizar pai do datablock atual
    {
        checkAndUpdateEmptyFathersAux(searchid, deep-1);
    }
}

void checkAndUpdateEmptyFathers(int searchid)
{
    BBranchBlock * fatherDatablock;
    int fatherIndex;

    bool found = false;

    BLeafBlock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    int deep = 0;

    while(true)
    {
        deep++;
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        fatherDatablock = branchBlock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            fatherIndex = i;
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }


    //Neste ponto temos a garantia de que o datablock que temos em mao eh um do tipo FOLHA
    BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;
    std::pair<uint32_t, uint32_t> * pair;
    for(int i = 0; i < leafBlock->getElements()->size(); i++)
    {
        pair = &((*leafBlock->getElements())[i]);
        if(searchid == pair->first)
        {
            int key = pair->second & 0xffff0000;
            key = key >> 16;
            int rowid = pair->second & 0xffff;
            Datablock * targetDatablock = DatablockLoader::loadDatablock(key);
            if(targetDatablock->type == 2) //data
            {
                found = true;
                break;
            }
            else
            {
                printf("Erro no acesso ao datablock na BTree, esperado DATA e foi recebido outro\n");
                break;
            }
        }
    }

    //Fim da busca

    if(!found)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar verificar datablock pai de uma folha. Folha não encontrada.\n");
        return;
    }

    //Folha encontrada, iremos atualizar os pais caso necessário
    if(fatherDatablock->size==0)
    {
        checkAndUpdateEmptyFathersAux(searchid, deep-1);
    }
}

BBranchBlock * getFatherDatablock(int searchid)
{
    BBranchBlock * fatherDatablock;

    bool found = false;

    BLeafBlock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        fatherDatablock = branchBlock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }


    //Neste ponto temos a garantia de que o datablock que temos em mao eh um do tipo FOLHA
    BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;
    std::pair<uint32_t, uint32_t> * pair;
    for(int i = 0; i < leafBlock->getElements()->size(); i++)
    {
        pair = &((*leafBlock->getElements())[i]);
        if(searchid == pair->first)
        {
            int key = pair->second & 0xffff0000;
            key = key >> 16;
            int rowid = pair->second & 0xffff;
            Datablock * targetDatablock = DatablockLoader::loadDatablock(key);
            if(targetDatablock->type == 2) //data
            {
                found = true;
                break;
            }
            else
            {
                printf("Erro no acesso ao datablock na BTree, esperado DATA e foi recebido outro\n");
                break;
            }
        }
    }

    //Fim da busca

    if(!found)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar verificar datablock pai de uma folha. Folha não encontrada.\n");
        return nullptr;
    }
    return fatherDatablock;
}

BBranchBlock * getFatherDatablock(BBranchBlock * branch)
{
    int searchid = branch->getKeys()->front();
    BBranchBlock * fatherDatablock = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    bool found = false;

    BBranchBlock * root = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock = root;


    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        fatherDatablock = branchBlock;
        branchBlock = (BBranchBlock *)currentDatablock;
        if(branchBlock->id == branch->id)
        {
            found = true;
            break;
        }
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }

    //Fim da busca

    if(!found)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar verificar datablock pai de uma folha. Folha não encontrada.\n");
        return nullptr;
    }
    return fatherDatablock;
}

BBranchBlock * getRightBranch(BBranchBlock * branch)
{
    //acha o branch
    //desce até folha indo sempre pelo da direita
    //pega sibbling da folha achada
    //sobe a quantidade que desceu
    int treeLevel = 0;
    bool foundBranch = false;
    int searchid = branch->getKeys()->front();

    BBranchBlock * root = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;


    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        if(branchBlock->id == branch->id)
        {
            foundBranch = true;
            break;
        }
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
        treeLevel++;
    }

    if(!foundBranch)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar achar datablock da direita de uma branch. Branch não encontrado.\n");
        return nullptr;
    }

    int nodesBellow = 0;
    //ACHAMOS O BRANCH, IREMOS DESCER ATÉ A FOLHA MAIS A DIREITA
    while(true)
    {
        int type = currentDatablock->type;
        if(type==3)
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        currentDatablock = DatablockLoader::loadDatablock((*branchBlock->getPointers())[branchBlock->getPointers()->size()-1]);
        nodesBellow++;
    }

    //PEGAMOS O IRMÃO DA FOLHA MAIS A DIREITA E SUBIMOS O MESMO NÚMERO DE VEZES QUE DESCEMOS PARA ACHAR O BRANCH INICIAL
    BLeafBlock * currentLeaf = (BLeafBlock *)currentDatablock;
    BLeafBlock rightSibbling = currentLeaf->getSibblingRight();
    BBranchBlock * rightBranch = getFatherDatablock(rightSibbling.getElements()->front().first);
    nodesBellow--;
    while(nodesBellow>0)
    {
        rightBranch = getFatherDatablock(rightBranch);
        nodesBellow--;
    }

    return rightBranch;
}

BBranchBlock * getLeftBranch(BBranchBlock * branch)
{
    //acha o branch
    //desce até folha indo sempre pelo da esquerda
    //pega sibbling da folha achada
    //sobe a quantidade que desceu
    int treeLevel = 0;
    bool foundBranch = false;
    int searchid = branch->getKeys()->front();

    BBranchBlock * root = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        if(branchBlock->id == branch->id)
        {
            foundBranch = true;
            break;
        }
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
        treeLevel++;
    }

    if(!foundBranch)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar achar datablock da direita de uma branch. Branch não encontrado.\n");
        return nullptr;
    }
    int nodesBellow = 0;
    //ACHAMOS O BRANCH, IREMOS DESCER ATÉ A FOLHA MAIS A DIREITA
    while(true)
    {
        int type = currentDatablock->type;
        if(type==3)
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        currentDatablock = DatablockLoader::loadDatablock((*branchBlock->getPointers())[0]);
        nodesBellow++;
    }
    //PEGAMOS O IRMÃO DA FOLHA MAIS A ESQUERDA E SUBIMOS O MESMO NÚMERO DE VEZES QUE DESCEMOS PARA ACHAR O BRANCH INICIAL
    BLeafBlock * currentLeaf = (BLeafBlock *)currentDatablock;
    BLeafBlock * leftSibbling = (BLeafBlock *)DatablockLoader::loadDatablock(currentLeaf->getSibblingLeft());
    BBranchBlock * leftBranch = getFatherDatablock(leftSibbling->getElements()->front().first);
    nodesBellow--;
    while(nodesBellow>0)
    {
        leftBranch = getFatherDatablock(leftBranch);
        nodesBellow--;
    }
    return leftBranch;
}

bool haveRightBranch(BBranchBlock * branch)
{
    bool foundBranch = false;
    int searchid = branch->getKeys()->front();

    BBranchBlock * root = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;


    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        if(branchBlock->id == branch->id)
        {
            foundBranch = true;
            break;
        }
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }

    if(!foundBranch)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar achar datablock da direita de uma branch. Branch não encontrado.\n");
        return false;
    }

    //ACHAMOS O BRANCH, IREMOS DESCER ATÉ A FOLHA MAIS A DIREITA
    while(true)
    {
        int type = currentDatablock->type;
        if(type==3)
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        currentDatablock = DatablockLoader::loadDatablock((*branchBlock->getPointers())[branchBlock->getPointers()->size()-1]);
    }

    BLeafBlock * currentLeaf = (BLeafBlock *)currentDatablock;
    if(currentLeaf->getSibblingRight() == 0)
    {
        return false;
    }
    return true;
}

bool haveLeftBranch(BBranchBlock * branch)
{
    bool foundBranch = false;
    int searchid = branch->getKeys()->front();

    BBranchBlock * root = (BBranchBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;


    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        if(branchBlock->id == branch->id)
        {
            foundBranch = true;
            break;
        }
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }

    if(!foundBranch)
    {
        //nao achou entrada para remover
        printf("Erro ao tentar achar datablock da esquerda de uma branch. Branch não encontrado.\n");
        return false;
    }

    //ACHAMOS O BRANCH, IREMOS DESCER ATÉ A FOLHA MAIS A ESQUERDA
    while(true)
    {
        int type = currentDatablock->type;
        if(type==3)
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        currentDatablock = DatablockLoader::loadDatablock((*branchBlock->getPointers())[0]);
    }

    BLeafBlock * currentLeaf = (BLeafBlock *)currentDatablock;
    if(currentLeaf->getSibblingLeft() == 0)
    {
        return false;
    }
    return true;
}

void BTree::checkAndUpdateFathersMinimum(int searchid)
{
    //checa se tá abaixo
    //tenta pegar emprestado da esquerda
    //tenta pegar emprestado da direita
    //merge com direita
    //se merge, checa o mesmo com o pai
    //se não merge, diminui um nível da árvore

    BBranchBlock * fatherDatablock = getFatherDatablock(searchid);
    BBranchBlock * currentDatablock = fatherDatablock;
    BBranchBlock * leftBranch;
    BBranchBlock * rightBranch;

    BBranchBlock * roote = (BBranchBlock *)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    uint16_t rootFirstChild = roote->getPointers()->front();

    //Folha encontrada, iremos atualizar os pais caso necessário
    while((currentDatablock->getKeys()->size()<(TESTSIZE*2/3))&&(currentDatablock->id!=(DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root)->id)))
    {
        if(haveRightBranch(currentDatablock))
            rightBranch = getRightBranch(currentDatablock);
        else
            rightBranch = 0;

        if(haveLeftBranch(currentDatablock)) {
            printf("left branch 1\n");
            leftBranch = getLeftBranch(currentDatablock);
            printf("left branch 2\n");
        }

        else
            leftBranch = 0;


        if(rightBranch!=0 && (!(rightBranch->getKeys()->size()-1<(TESTSIZE*2/3))))
        {
            //ROUBA ENTRY MENOR DO BRANCH DA DIREITA
            unsigned int firstElement = (*(rightBranch->getKeys()))[0];

            //REMOVER PRIMEIRA ENTRY DO BRANCH DA DIREITA
            std::vector<uint32_t > * newRightElements = rightBranch->getKeys();
            newRightElements->erase(newRightElements->begin());
            rightBranch->setKeys(*newRightElements);

            //REMOVER PRIMEIRO POINTER DO BRANCH DA DIREITA
            std::vector<uint16_t > * newRightPointer = rightBranch->getPointers();
            newRightPointer->erase(newRightPointer->begin());
            rightBranch->setPointers(*newRightPointer);

            uint16_t firstPointer = rightBranch->getPointers()->at(0);
            uint16_t secondPointer = rightBranch->getPointers()->at(1);
            currentDatablock->addEntry(firstPointer,firstElement,secondPointer);

            //ATUALIZAR PAI DO BRANCH DA DIREITA
            //updateFather(rightBranch);
            updateTree();
        }
        else if(leftBranch!=0 && (!(leftBranch->getKeys()->size()-1<(TESTSIZE*2/3))))
        {
            //ROUBA ENTRY MAIOR DO BRANCH DA ESQUERDA
            unsigned int lastElement = (*(leftBranch->getKeys()))[leftBranch->size];

            //REMOVER ULTIMA ENTRY DO BRANCH DA ESQUERDA
            std::vector<uint32_t > * newLeftElements = leftBranch->getKeys();
            newLeftElements->erase(newLeftElements->begin()+newLeftElements->size());
            leftBranch->setKeys(*newLeftElements);

            //REMOVER ULTIMO POINTER DO BRANCH DA ESQUERDA
            std::vector<uint16_t > * newLeftPointer = leftBranch->getPointers();
            newLeftPointer->erase(newLeftPointer->begin()+newLeftPointer->size()-1);
            leftBranch->setPointers(*newLeftPointer);

            uint16_t firstPointer = leftBranch->getPointers()->at(leftBranch->getPointers()->size()-1);
            uint16_t secondPointer = leftBranch->getPointers()->at(leftBranch->getPointers()->size()-2);
            currentDatablock->addEntry(firstPointer,lastElement,secondPointer);

            //ATUALIZAR PAI DO BRANCH DA DIREITA
            //updateFather(leftBranch);
            updateTree();
        }
        else
        {
            BBranchBlock * root = (BBranchBlock *)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

            if(rightBranch!=0)
            {
                //PEGAR ENTRADAS DO NODO DA DIREITA E COLOCAR NO ATUAL
                std::vector<uint32_t> elementsVector = *(rightBranch->getKeys());
                for (int i = 0; i < rightBranch->getKeys()->size(); i++) {
                    uint32_t currentElement = elementsVector[i];

                    currentDatablock->addEntry(rightBranch->getPointers()->at(i), currentElement, rightBranch->getPointers()->at(i+1));
                }
                currentDatablock = getFatherDatablock(rightBranch);

                //REMOVER PAI DO NODO DA DIREITA
                removeFather(rightBranch);
                updateTree();
            }
            else if(leftBranch!=0)
            {
                //PEGAR ENTRADAS DO NODO DA ESQUERDA E COLOCAR NO ATUAL
                std::vector<uint32_t> elementsVector = *(leftBranch->getKeys());
                for (int i = 0; i < leftBranch->getKeys()->size(); i++) {
                    uint32_t currentElement = elementsVector[i];

                    currentDatablock->addEntry(leftBranch->getPointers()->at(i), currentElement, leftBranch->getPointers()->at(i+1));
                }

                currentDatablock = getFatherDatablock(leftBranch);

                //REMOVER PAI DO NODO DA ESQUERDA

                removeFather(leftBranch);
                updateTree();
            }
        }
    }
    BBranchBlock * rooti = (BBranchBlock *)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);
    if(rooti->getKeys()->size() == 0)
    {
        Datablock * newRoot = DatablockLoader::loadDatablock(rootFirstChild);
        if(newRoot->type == 3)
        {
            DatablockLoader::masterBlock->root = rootFirstChild;
            BLeafBlock * leafRoot = (BLeafBlock *) DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);
            leafRoot->setSibblingLeft(0);
            leafRoot->setSibblingRight(0);
        }
        else
        {
            DatablockLoader::masterBlock->root = rootFirstChild;
        }
    }
}

bool BTree::removeEntry(int searchid)
{
    BBranchBlock * rightSibblingDatablockFather;
    int rightSibblingFatherIndex = 0;
    BBranchBlock * fatherDatablock;
    int fatherIndex;
    int entryIndex;

    bool found = false;

    //ACHAR ENTRY
    BLeafBlock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        fatherDatablock = branchBlock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            fatherIndex = i;
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }


    //Neste ponto temos a garantia de que o datablock que temos em mao eh um do tipo FOLHA
    BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;
    std::pair<uint32_t, uint32_t> * pair;
    for(int i = 0; i < leafBlock->getElements()->size(); i++)
    {
        entryIndex = i;
        pair = &((*leafBlock->getElements())[i]);
        if(searchid == pair->first)
        {
            int key = pair->second & 0xffff0000;
            key = key >> 16;
            int rowid = pair->second & 0xffff;
            Datablock * targetDatablock = DatablockLoader::loadDatablock(key);
            if(targetDatablock->type == 2) //data
            {
                found = true;
                break;
            }
            else
            {
                printf("Erro no acesso ao datablock na BTree, esperado DATA e foi recebido outro\n");
                break;
            }
        }
    }

    //Fim da busca

    if(!found)
    {
        //nao achou entrada para remover
        return false;
    }


    BLeafBlock * rightSibbling;
    if(leafBlock->getSibblingRight() != 0)
    {
        rightSibbling = (BLeafBlock*)DatablockLoader::loadDatablock(leafBlock->getSibblingRight());
    }
    else
    {
        rightSibbling = 0;
    }

    BLeafBlock * leftSibbling;
    if(leafBlock->getSibblingLeft() !=0)
    {
        leftSibbling = (BLeafBlock*)DatablockLoader::loadDatablock(leafBlock->getSibblingLeft());
    }
    else
    {
        leftSibbling = 0;
    }


    if((leafBlock->getElements()->size()-1 < (TESTSIZE*2/3)) && (leafBlock->id != root->id))
    {
        //quando remover, ficará menor que 1/2, ou seja, precisa pegar emprestado de vizinhos

        if(rightSibbling!=0 && (rightSibbling->getElements()->size()-1>=(TESTSIZE*2/3)))
        {
            std::pair<uint32_t, uint32_t> toRemove = leafBlock->getElements()->at(entryIndex);
            TableBlock * blockEntry = (TableBlock*)DatablockLoader::loadDatablock((toRemove.second&0xffff0000)>>16);
            blockEntry->removeEntry(toRemove.second);
            DatablockLoader::saveDatablock(blockEntry);

            leafBlock->removeEntry(entryIndex);

            //ROUBA ENTRY MENOR DO NODO DA DIREITA
            std::pair<uint32_t, uint32_t> firstElement = (*(rightSibbling->getElements()))[0];

            //REMOVER PRIMEIRA ENTRY DO NODO DA DIREITA
            std::vector<std::pair<uint32_t, uint32_t>> * newRightElements = rightSibbling->getElements();
            newRightElements->erase(newRightElements->begin());
            rightSibbling->setElements(*newRightElements);

            leafBlock->addEntry(firstElement.first, firstElement.second);

            //ATUALIZAR PAI DO NODO DA DIREITA
            //updateFather(rightSibbling->getElements()->front().first);
            updateTree();
            /*std::vector<uint32_t> rightSibblingDatablockFatherNew = *rightSibblingDatablockFather->getKeys();
            uint32_t newRightKey = rightSibbling->getElements()->front().first;
            rightSibblingDatablockFatherNew.at(rightSibblingFatherIndex) = newRightKey;
            rightSibblingDatablockFather->setKeys(rightSibblingDatablockFatherNew);*/

            return true;
        }

        else if(leftSibbling!=0 && (leftSibbling->getElements()->size()-1>=(TESTSIZE*2/3)))
        {
            std::pair<uint32_t, uint32_t> toRemove = leafBlock->getElements()->at(entryIndex);
            TableBlock * blockEntry = (TableBlock*)DatablockLoader::loadDatablock((toRemove.second&0xffff0000)>>16);
            blockEntry->removeEntry(toRemove.second);
            DatablockLoader::saveDatablock(blockEntry);

            leafBlock->removeEntry(entryIndex);

            //ROUBA ENTRY MAIS ALTA DO NODO DA ESQUERDA
            unsigned long lastElementPosition = leftSibbling->getElements()->size()-1;
            std::pair<uint32_t, uint32_t> lastElement = (*(leftSibbling->getElements()))[lastElementPosition];

            //REMOVER ULTIMA ENTRY DO NODO DA ESQUERDA
            std::vector<std::pair<uint32_t, uint32_t>> * newLeftElements = leftSibbling->getElements();
            newLeftElements->pop_back();
            leftSibbling->setElements(*newLeftElements);

            leafBlock->addEntry(lastElement.first, lastElement.second);

            //ATUALIZAR PAI
            std::vector<uint32_t> fatherDatablockNew = *fatherDatablock->getKeys();
            uint32_t newKey = leafBlock->getElements()->front().first;
            fatherDatablockNew.at(fatherIndex) = newKey;
            fatherDatablock->setKeys(fatherDatablockNew);

            return true;
        }

        else
        {
            //sibbling da esquerda nao pode emprestar, entao iremos remover e fazer o merge com o da direita, se possivel. Se nao, com o da esquerda

            std::pair<uint32_t, uint32_t> toRemove = leafBlock->getElements()->at(entryIndex);
            TableBlock * blockEntry = (TableBlock*)DatablockLoader::loadDatablock((toRemove.second&0xffff0000)>>16);
            blockEntry->removeEntry(toRemove.second);
            DatablockLoader::saveDatablock(blockEntry);

            leafBlock->removeEntry(entryIndex);

            if(rightSibbling!=0)
            {
                //PEGAR ENTRADAS DO NODO DA DIREITA E COLOCAR NO ATUAL
                std::vector<std::pair<uint32_t, uint32_t>> elementsVector = *(rightSibbling->getElements());
                for(int i = 0; i < rightSibbling->getElements()->size(); i++)
                {
                    std::pair<uint32_t, uint32_t> currentElement = elementsVector[i];

                    leafBlock->addEntry(currentElement.first, currentElement.second);
                }

                //ATENCAO: nao remover esse print
                printf("    ");
                //serio

                //ATUALIZAR SIBBLINGS
                BLeafBlock * rightLeftSibbling;
                //rightLeftSibbling = (BLeafBlock*)DatablockLoader::loadDatablock(rightSibbling->getSibblingLeft());
                rightLeftSibbling = leafBlock;

                BLeafBlock * rightRightSibbling;
                if(rightSibbling->getSibblingRight())
                {
                    rightRightSibbling = (BLeafBlock*)DatablockLoader::loadDatablock(rightSibbling->getSibblingRight());
                }
                else
                {
                    rightRightSibbling = 0;
                }

                if(rightRightSibbling != 0)
                {
                    rightLeftSibbling->setSibblingRight(rightRightSibbling->id);
                    rightRightSibbling->setSibblingLeft(rightLeftSibbling->id);
                }
                else
                {
                    rightLeftSibbling->setSibblingRight(0);
                }

                //REMOVER PAI DO NODO DA ESQUERDA
                removeFather(rightSibbling->getElements()->front().first);
                updateTree();

                //Checar se pais de pais não estão abaixo do minimo (1/2)
                checkAndUpdateFathersMinimum(rightSibbling->getElements()->front().first);

                return true;
            }

            else if(leftSibbling!=0)
            {
                //PEGAR ENTRADAS DO NODO DA ESQUERDA E COLOCAR NO ATUAL
                std::vector<std::pair<uint32_t, uint32_t>> elementsVector = *(leftSibbling->getElements());
                for(int i = 0; i < leftSibbling->getElements()->size(); i++)
                {
                    std::pair<uint32_t, uint32_t> currentElement = elementsVector[i];

                    leafBlock->addEntry(currentElement.first, currentElement.second);
                }
                //ATUALIZAR SIBBLINGS
                BLeafBlock * leftRightSibbling;
                leftRightSibbling = (BLeafBlock*)DatablockLoader::loadDatablock(leftSibbling->getSibblingRight());


                BLeafBlock * leftLeftSibbling;
                if(leftSibbling->getSibblingLeft())
                {
                    leftLeftSibbling = (BLeafBlock*)DatablockLoader::loadDatablock(leftSibbling->getSibblingLeft());
                }
                else
                {
                    leftLeftSibbling = 0;
                }

                if(leftLeftSibbling != 0)
                {
                    leftRightSibbling->setSibblingLeft(leftLeftSibbling->id);
                    leftLeftSibbling->setSibblingRight(leftRightSibbling->id);
                }
                else
                {
                    leftRightSibbling->setSibblingLeft(0);
                }
                //REMOVER PAI DO NODO DA ESQUERDA
                removeFather(leftSibbling->getElements()->front().first);
                updateTree();

                //Checar se pais de pais não estão abaixo do minimo (1/2)
                checkAndUpdateFathersMinimum(leftSibbling->getElements()->front().first);

                return true;
            }
        }
    }

    else
    {
        std::pair<uint32_t, uint32_t> toRemove = leafBlock->getElements()->at(entryIndex);
        TableBlock * blockEntry = (TableBlock*)DatablockLoader::loadDatablock((toRemove.second&0xffff0000)>>16);
        blockEntry->removeEntry(toRemove.second);
        DatablockLoader::saveDatablock(blockEntry);

        leafBlock->removeEntry(entryIndex);
        //printf("%d\n", leafBlock->getElements()->front().first);
        //printf("%d\n",entryIndex);
        if(entryIndex == 0 && (leafBlock->id != root->id))
        {
            std::vector<uint32_t> fatherDatablockNew = *fatherDatablock->getKeys();
            uint32_t newKey = leafBlock->getElements()->at(0).first;
            fatherDatablockNew.at(fatherIndex) = newKey;
            fatherDatablock->setKeys(fatherDatablockNew);
        }

        return true;
    }
    DatablockLoader::saveDatablock(leafBlock);
    DatablockLoader::saveDatablock(branchBlock);
    DatablockLoader::saveDatablock(fatherDatablock);
    DatablockLoader::saveDatablock(leftSibbling);
    DatablockLoader::saveDatablock(rightSibbling);
    DatablockLoader::saveDatablock(rightSibblingDatablockFather);
    DatablockLoader::saveDatablock(currentDatablock);
}

std::string BTree::findRegistry(int searchid)
{
    Datablock * root = (BLeafBlock*)DatablockLoader::loadDatablock(DatablockLoader::masterBlock->root);

    Datablock * currentDatablock = root;
    BBranchBlock * branchBlock;

    bool found = false;

    while(true)
    {
        int type = currentDatablock->type;
        if(type == 3) //Se tipo for leaf
        {
            break;
        }
        branchBlock = (BBranchBlock *)currentDatablock;
        uint16_t pointer = 0;
        uint32_t key = 0;
        for(int i = 0; i < branchBlock->getKeys()->size(); i++)
        {
            key = (*branchBlock->getKeys())[i];
            if(searchid < key)
            {
                pointer = (*branchBlock->getPointers())[i];
                break;
            }
            if(searchid >= key)
            {
                continue;
            }
        }
        if(pointer == 0) //se nao achamos a chave, deve ser o ultimo
        {
            pointer = (*branchBlock->getPointers())[branchBlock->getPointers()->size()-1];
        }
        currentDatablock = DatablockLoader::loadDatablock(pointer);
    }


    //Neste ponto temos a garantia de que o datablock que temos em mao eh um do tipo FOLHA
    BLeafBlock * leafBlock = (BLeafBlock *)currentDatablock;
    std::pair<uint32_t, uint32_t> * pair;
    for(int i = 0; i < leafBlock->getElements()->size(); i++)
    {
        pair = &((*leafBlock->getElements())[i]);
        if(searchid == pair->first)
        {
            int key = pair->second & 0xffff0000;
            key = key >> 16;
            int rowid = pair->second & 0xffff;
            Datablock * targetDatablock = DatablockLoader::loadDatablock(key);
            if(targetDatablock->type == 2) //data
            {
                TableBlock * tableBlock = (TableBlock *)targetDatablock;
                return tableBlock->getRegistry(rowid);
            }
            else
            {
                printf("Erro no acesso ao datablock na BTree, esperado DATA e foi recebido outro");
                return "";
            }
            //return
        }
    }

    return "";
}
