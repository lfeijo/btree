#include <iostream>
#include "Datablocks/BLeafBlock.h"
#include "DatablockLoader.h"
#include "BTree.h"
#include "Parser/CommandParser.h"
#include <cstring>
#include "stdio.h"
#include "Datablocks/TableBlock.h"
#include "DatablockManager.h"

using namespace std;

void nInsert(int n)
{
    static const char alphanum[] =
            "0123456789"
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    "abcdefghijklmnopqrstuvwxyz";

    std::string s = "";
    uint32_t num = 0;



    while(n > 0)
    {
        for (int i = 0; i < 19; ++i) {
            s += alphanum[rand() % (sizeof(alphanum) - 1)];
        }

        num = rand() % 10000;

        DatablockManager::addEntry(num, s);

        s = "";

        n--;
    }
}

int main(int argc, char** argv) {

    if (argc>2) {
        if (strcmp(argv[1], "new")==0) {
            if (DatablockLoader::newDatafile(argv[2])) {
                printf("OK File %s created.", argv[2]);
                BLeafBlock *root = new BLeafBlock(DatablockManager::getUniqueId());
                DatablockLoader::saveDatablock(root);
                DatablockLoader::masterBlock->root = root->id;
                //DatablockLoader::masterBlock->root = 0;
            } else {
                printf("ERR Error creating file %s.", argv[2]);
                exit(1);
            }
        }
        if (strcmp(argv[1], "load")==0) {
            if (DatablockLoader::loadDatafile(argv[2])){
                printf("OK File %s loaded.", argv[2]);
            } else {
                printf("ERR Error loading file %s.", argv[2]);
                exit(1);
            }
        }
        string line;
        printf("\n");



        while (true) {
            cin.clear();
            cin.sync();
            printf("$ ");
            getline(cin, line);
            Command cmdObj = CommandParser::getCommand(line);
            if (!cmdObj.error) {
                if (cmdObj.command == Command::SELECT) {
                    if (cmdObj.key != 0) { // select por pk
                        // usar indice
                        printf("%s",DatablockManager::findByIndex(cmdObj.key).c_str());
                    } else { // select por valor
                        std::vector<std::tuple<uint32_t, uint32_t, std::string>> res = DatablockManager::findByValue(cmdObj.value);
                        if (res.size()>0) {
                            for (auto r : res) {
                                printf("%d: %s\n", get<1>(r), get<2>(r).c_str()); // leve vomito ao usar essa sintaxe
                            }
                        }
                        printf("OK\n");
                    }
                } else
                if (cmdObj.command == Command::INSERT) {
                    if(cmdObj.value == "")
                    {
                        nInsert(cmdObj.key);
                        BTree tree = BTree();
                        tree.printTree();
                        printf("OK\n");
                    }
                    else
                    if (DatablockManager::addEntry(cmdObj.key, cmdObj.value)!=0) {
                        BTree tree = BTree();
                        tree.printTree();
                        printf("OK\n");
                    }
                } else
                if (cmdObj.command == Command::UPDATE) {
                    DatablockManager::removeEntry(cmdObj.key);
                    DatablockManager::addEntry(cmdObj.key, cmdObj.value);
                    printf("OK\n");

                } else
                if (cmdObj.command == Command::DELETE) {
                    DatablockManager::removeEntry(cmdObj.key);
                    BTree tree = BTree();
                    tree.printTree();
                    printf("OK\n");
                    printf("////\n");
                    //if (DatablockManager::deleteEntry(cmdObj.key)!=0) {

                    //}
                } else
                if (cmdObj.command == Command::COMMIT) {
                    if (DatablockLoader::flushBuffer()) { // salva todos datablocks do buffer em disco mas nao os deleta do buffer.
                        printf("OK\n");
                    } else {
                        printf("ERR\n");
                    }
                }
            } else {
                for (int i = 0; i < cmdObj.errorPosition+2; ++i) {
                    printf("-");
                }
                printf("^\n");
                printf("ERR Syntax error: %s\n", cmdObj.errorType.c_str());
            }
        }
    } else {
        printf("ERR Wrong usage.\n");
    }

    return 0;
}
