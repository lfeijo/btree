//
// Created by Lucas Feijo on 5/15/16.
//

#ifndef V1_DATABLOCKBUFFER_H
#define V1_DATABLOCKBUFFER_H


#include <map>
#include <list>
#include "Datablocks/Datablock.h"

class DatablockBuffer {
public:
    const int maxSize = 256;

    Datablock * getDatablock(int id);
    void storeDatablock(Datablock * datablock);
    bool flush();
private:
    // guarda datablocks em ordem FIFO
    static std::list<Datablock *>* getDatablockBuffer() {
        static std::list<Datablock *> *instance = new std::list<Datablock *>();
        return instance;
    }
};

#endif //V1_DATABLOCKBUFFER_H
